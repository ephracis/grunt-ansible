# grunt-ansible

Grunt plugin for running Ansible

## Getting Started

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out
the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains
how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as
install and use Grunt plugins. Once you're familiar with that process, you may
install this plugin with this command:

```shell
npm install grunt-foobar --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with
this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-foobar');
```

## Ansible task

_Run this task with the `grunt ansible` command._

Task targets, files and options may be specified according to the grunt
[Configuring tasks](http://gruntjs.com/configuring-tasks) guide.

### Options

#### args

Type: `Object`

The command line arguments to send to `ansible-playbook`. Cannot be in short
form.

## Example

Each target should correspond to a playbook. The options under each target
is turned into command line arguments. For example:

```js
ansible: {
  playbook_1: {
    args: {
      become: true,
      user: 'bob',
      inventory: '/etc/hosts'
    }
  },
  playbook_2: {
    inventory: 'localhost,'
    connection: local,
  }
}
```

The playbooks can then be run with:

```shell
$ grunt ansible:playbook_1
...

$ grunt ansible:playbook_2
...
```

It is also possible to send arguments via the command line like so:

```shell
$ grunt ansible:playbook --become --user=bob
...
```
