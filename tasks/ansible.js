/**
 * Module with the grunt-foobar plugin
 *
 * @module grunt-foobar
 * @author Basalt AB
 * @license
 * Copyright (c) 2018 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

const execSync = require('child_process').execSync;

module.exports = function(grunt) {
  var AnsiblePlaybook = require('./lib/ansiblePlaybook');
  var ansiblePlaybook = new AnsiblePlaybook();

  grunt.registerTask('ansible', 'Run ansible-playbook.', function(playbook) {
    ansiblePlaybook.flags = grunt.option.flags();

    if (playbook in grunt.config.data.ansible)
      ansiblePlaybook.config = grunt.config.data.ansible[playbook].args;

    var cmd = ansiblePlaybook.cmd(playbook);
    grunt.log.ok(cmd);
    execSync(cmd, {stdio:[0,1,2]});
  });
};
