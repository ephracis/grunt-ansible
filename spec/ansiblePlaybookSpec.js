'use strict';

describe("AnsiblePlaybook", function() {
  var ansible;
  var AnsiblePlaybook = require('../tasks/lib/ansiblePlaybook.js');

  beforeEach(function() {
    ansible = new AnsiblePlaybook();
  });

  describe(".cmd", function() {

    it("should add extension if needed", function() {
      expect(ansible.cmd('foo.yml')).toEqual('ansible-playbook foo.yml');
      expect(ansible.cmd('foo.yaml')).toEqual('ansible-playbook foo.yaml');
      expect(ansible.cmd('foo')).toEqual('ansible-playbook foo.yml');
    });

    it("should add arguments", function() {
      ansible.config = { user: 'bob', become: true };
      expect(ansible.cmd('foo'))
        .toEqual('ansible-playbook --become --user=bob foo.yml');
    });

  });

  describe(".flags", function() {
    it("should filter grunt flags", function() {
      ansible.flags = ['--stack', '--base', '--gruntfile=test', '--user=bob'];
      expect(ansible.args).toEqual(['--user=bob']);
    });
  });

  describe(".config", function() {
    it("should generate command line args", function() {
      ansible.config = { user: 'bob', become: true };
      expect(ansible.args).toEqual(['--become', '--user=bob']);
    });

    it("should skip false values", function() {
      ansible.config = { user: 'bob', become: false };
      expect(ansible.args).toEqual(['--user=bob']);
    });
  });

});
