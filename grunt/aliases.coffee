module.exports = (grunt) ->
  release: (action) ->
    bump_action = if action then action else 'minor'
    grunt.task.run [
      "bump-only:#{bump_action}",
      'shell:changelog',
      'bump-commit'
    ]
