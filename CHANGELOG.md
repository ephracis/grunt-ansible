# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.0] - 2018-09-13
### Added
- Specify Ansible CLI arguments via either the config or as flags passed to grunt.

[0.1.0]: https://gitlab.com/basalt/grunt-ansible/compare/8c31edc...0.1.0
